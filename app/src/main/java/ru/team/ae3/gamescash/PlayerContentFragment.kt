package ru.team.ae3.gamescash

import android.os.*
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.bartoszlipinski.recyclerviewheader2.RecyclerViewHeader



class PlayerContentFragment : Fragment() {

    var list : RecyclerView? = null
    var adapter = PlayerAdapter()

    var header: RecyclerViewHeader? = null

    var headerPlayerName : TextView? = null
    var headerPlayerBalance : TextView? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {


        val view = inflater!!.inflate(R.layout.fragment_game_list, container, false)
        list = view.findViewById(R.id.game_recycler_view) as RecyclerView

        headerPlayerName = view.findViewById(R.id.header_player_name) as TextView
        headerPlayerBalance = view.findViewById(R.id.header_player_balance) as TextView

//        for (i in 0..4) {
//            val eachParentItem = SubcategoryParentListItem()
//            subcategoryParentListItems.add(eachParentItem)
//        }

        /*val childItemList = ArrayList<SubcategoryChildListItem>()

        var parentListItems = ArrayList<ParentListItem>();
        subcategoryParentListItems.forEach {
            childItemList.add(SubcategoryChildListItem())

            it.childItemList = childItemList
            parentListItems.add(it)
        }*/


        list!!.layoutManager = LinearLayoutManager(context)
        list!!.adapter = adapter

        list?.setRecyclerListener {
            if(adapter!!.data.isNotEmpty()) {
                headerPlayerName!!.text = adapter!!.data.first().name
                headerPlayerBalance!!.text = adapter!!.data.first().balance.toString()
            }
        }

        header = view.findViewById(R.id.header) as RecyclerViewHeader
        header!!.attachTo(list!!)

        return view
    }

    override fun onResume() {
        super.onResume()
    }

}
