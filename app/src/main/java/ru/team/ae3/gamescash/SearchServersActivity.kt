package ru.team.ae3.gamescash

import android.support.v4.app.Fragment
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.*
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ProgressBar
import com.stepstone.stepper.Step
import com.stepstone.stepper.VerificationError
import kotlin.concurrent.timer

class SearchServersActivity : Fragment(), Step {
    override fun onSelected() {

    }

    override fun verifyStep(): VerificationError {
        return VerificationError("Permanent Error")
    }

    override fun onError(error: VerificationError) {

    }

    companion object{
        val MSG_DATA_CHANGED = 1
    }

    private var list: RecyclerView? = null
    private var swipeRefresh: SwipeRefreshLayout? = null
    private var progressBar: ProgressBar? = null

    var mClientService: SearchServerService? = null
    var mClientBound = false

    var serviceIntent : Intent? = null

    var adapter = ServerAdapter()
    val mMessenger = Messenger(IncomingHandler())
    var mService: Messenger? = null

    private val mClientConnection = object : ServiceConnection {

        override fun onServiceConnected(className: ComponentName,
                                        service: IBinder) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            //val binder = service as SearchServerService.SearchServerBinder
            mClientBound = true
            mService = Messenger(service)
            try {
                var msg = Message()
                msg.what = SearchServerService.MSG_REGISTER_CLIENT
                msg.replyTo = mMessenger
                mService!!.send(msg)

                msg = Message()
                msg.what = SearchServerService.START_SEARCHING
                mService!!.send(msg)
            } catch (e:RemoteException ) {
                Log.i("Client Msg", e.toString())
            }

            progressBar!!.visibility = View.GONE
            list!!.visibility = View.VISIBLE
            list!!.adapter = adapter
            list!!.adapter.notifyDataSetChanged()
/*           mClientService!!.startSearchingService()

           var data = mClientService!!.ServerList
           adapter.data = data
           progressBar!!.visibility = View.GONE
           list!!.visibility = View.VISIBLE

           list!!.adapter = adapter
           list!!.adapter.notifyDataSetChanged()*/
       }

       override fun onServiceDisconnected(arg0: ComponentName) {
           mClientBound = false
       }
   }

   override fun onCreate(savedInstanceState: Bundle?) {
       super.onCreate(savedInstanceState)


   }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val view = inflater!!.inflate(R.layout.activity_search_servers, container, false)

        list = view?.findViewById(R.id.list_servers) as RecyclerView
        swipeRefresh = view.findViewById(R.id.swipe_refresh) as SwipeRefreshLayout
        progressBar = view.findViewById(R.id.progress_bar_server) as ProgressBar

        serviceIntent = Intent(activity.applicationContext, SearchServerService::class.java)
        serviceIntent!!.putExtra(SearchServerService.MESSENGER, mMessenger)
        activity.bindService(serviceIntent, mClientConnection, Context.BIND_AUTO_CREATE)


        list!!.layoutManager = LinearLayoutManager(activity.applicationContext)


        swipeRefresh!!.setOnRefreshListener {
            var msg = Message()
            msg.what = SearchServerService.START_SEARCHING
            mService!!.send(msg)
            timer("timer", false, 5000.toLong(), 5000.toLong(), {
                swipeRefresh!!.post { swipeRefresh!!.isRefreshing = false }
            })
        }



        return view
    }

    override fun onStop() {
       super.onStop()
       activity.stopService(serviceIntent)
       if (mClientBound) {
           activity.unbindService(mClientConnection)
           mClientBound = false
       }
   }

   internal inner class IncomingHandler : Handler(){
       override fun handleMessage(msg: Message?) {
           Log.i("Client Msg",msg!!.what.toString())
           when (msg.what) {
               MSG_DATA_CHANGED -> {
                   adapter.data = (msg.obj as SearchServerService).ServerList
                   list!!.adapter = adapter
                   list!!.adapter.notifyDataSetChanged()
               }
               else -> super.handleMessage(msg)
           }
       }
   }


    fun getPlayerName(){
        return
    }
}
