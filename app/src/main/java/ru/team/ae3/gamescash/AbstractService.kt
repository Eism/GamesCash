package ru.team.ae3.gamescash

import android.app.Notification
import android.app.Service
import android.os.Handler
import android.os.Message
import android.os.Messenger
import android.util.Log
import android.app.NotificationManager
import android.content.Intent
import android.app.PendingIntent
import java.util.*


/**
 * Created by Ericos on 10.03.2017.
 */


abstract class AbstractService : Service() {
    enum class EServiceMessages{
        MSG_DATA_CHANGED,
        MSG_ARE_GETTING_SERVICE,
        MSG_END_GAME,
        MSG_START_GAME,
        MSG_IS_NOT_START_GAME,
        MSG_IS_ACCEPTED
    }
    enum class EActivityMessages{
        MSG_REGISTER_CLIENT,
        MSG_UNREGISTER_CLIENT,
        MSG_SERVICE,
        MSG_START_GAME,
        MSG_STOP_SERVICE
    }

    var bGameIsStarted = false

    protected var mServerRunning = false

    abstract var mActivity: Messenger?

    val mMessenger = Messenger(IncomingHandler())

    var clientList = ArrayList<Player>()

    var operationList = ArrayList<Operation>()

    val bank = Player(Player.ERole.ROLE_Bank, this)

    fun onDataChanged(){
        val message = Message()
        message.what = EServiceMessages.MSG_DATA_CHANGED.ordinal
        mActivity!!.send(message)
    }

    override fun onCreate() {
        super.onCreate()
        mServerRunning = true
    }

    fun multicastSend(player: Player?, message : String, including : Boolean){
        clientList
                .filter { if(including) { true } else { player != it }}
                .forEach { it.sendMessage(message) }
    }

    override fun onDestroy() {
        super.onDestroy()
        mServerRunning = false
    }

    internal inner class IncomingHandler : Handler(){
        override fun handleMessage(msg: Message?) {
            Log.i("Client Msg",msg!!.what.toString())
            when (msg.what) {
                EActivityMessages.MSG_REGISTER_CLIENT.ordinal -> {
                    mActivity = msg.replyTo
                    Log.i("Client Msg","Get MSG_REGISTER_CLIENT")
                }
                EActivityMessages.MSG_UNREGISTER_CLIENT.ordinal -> {
                    mActivity = null
                }
                EActivityMessages.MSG_SERVICE.ordinal -> {
                    val message = Message()
                    message.what = AbstractService.EServiceMessages.MSG_ARE_GETTING_SERVICE.ordinal
                    message.obj = this@AbstractService
                    mActivity!!.send(message)
                }
                EActivityMessages.MSG_START_GAME.ordinal ->{
                    val message = Message()
                    message.what = AbstractService.EServiceMessages.MSG_START_GAME.ordinal
                    message.obj = this@AbstractService
                    mActivity!!.send(message)
                    multicastSend(null, "Game is started", true)
                    bGameIsStarted = true
                }
                EActivityMessages.MSG_STOP_SERVICE.ordinal ->{
                    bGameIsStarted = false
                    stopAService()
                }
                else -> super.handleMessage(msg)
            }
        }
    }

    open fun stopAService() {
        clientList.forEach(Player::closeClient)
        clientList.clear()
        stopSelf()
    }
}