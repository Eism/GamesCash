package ru.team.ae3.gamescash

import android.app.Service
import android.content.Context
import android.content.Intent
import android.net.wifi.WifiManager
import android.os.Binder
import android.os.IBinder
import android.os.Messenger
import android.util.Log
import ru.team.ae3.gamescash.Player
import java.io.*
import java.net.*
import java.util.*
import kotlin.concurrent.timerTask



class ServerService : AbstractService() {
    private var mUdpSocket : DatagramSocket? = null
    var udpPort = 25566

    private var mTcpSocket : ServerSocket? = null
    var tcpPort = 30015

    override var mActivity: Messenger? = null

    override fun onCreate() {

    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        super.onCreate()
        try {
            mUdpSocket = DatagramSocket(udpPort)
            mUdpSocket!!.soTimeout = 5000
            mUdpSocket!!.broadcast = true
            mTcpSocket = ServerSocket(tcpPort)
        }catch (e : SocketException){

        }
        startSearchingResponseService()
        clientList.clear()
        Player(Player.ERole.ROLE_Authority,this)

        clientList.first().name = "Server"
        clientList.first().bIsAccepted = true
        startInputConnectionService()
        return super.onStartCommand(intent, flags, startId)
    }

    override fun onBind(p0: Intent?): IBinder {
        return mMessenger.binder
    }

    private fun startInputConnectionService(){
        Thread(Runnable {
            while (mServerRunning) {
                try {
                    val socket = mTcpSocket!!.accept()
                    if (socket != null){
                        Player(Player.ERole.ROLE_Authority, this, socket)
                    }
                } catch (e : SocketException) {

                }
            }
        }).start()
    }

    private fun startSearchingResponseService() {
        Thread(Runnable {
            while (mServerRunning) {
                var buf = ByteArray(13)
                var packet = DatagramPacket(buf, buf.size)
                try {
                    mUdpSocket!!.receive(packet)
                    Log.i("SRS",String(packet.data))
                    if (String(packet.data) == "Search server") {
                        Log.i("SRS",packet.address.toString() + " " + packet.port)
                        val str = ("I server " + clientList.first().name).toByteArray()
                        mUdpSocket!!.send(DatagramPacket(str, str.size, packet.address, packet.port))
                    }
                } catch (e: SocketTimeoutException) {
                    //e.printStackTrace()
                } catch (e: SocketException){

                }

            }
        }).start()
    }

    fun sendMoney(player: Player, count : Int){
        player.balance += count
        onDataChanged()
        operationList.add(Operation(bank,player,count))
        multicastSend(player,"Operation: send money From: Bank" +
                " To: " + player.uuid.toString() +
                " Count: " + count.toString(), true)
    }

    fun sendMoneyAllPlayers(count: Int){
        clientList.forEach {
            it.balance += count
            onDataChanged()
            operationList.add(Operation(bank,it,count))
            multicastSend(it,"Operation: send money From: Bank" +
                    " To: " + it.uuid.toString() +
                    " Count: " + count.toString(), true)
        }
    }

    override fun stopAService() {
        mUdpSocket!!.close()
        mTcpSocket!!.close()
        super.stopAService()
    }


    override fun onDestroy() {
        super.onDestroy()
        mServerRunning = false
    }


}
