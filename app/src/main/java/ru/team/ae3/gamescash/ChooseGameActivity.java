package ru.team.ae3.gamescash;

import android.support.v4.app.Fragment;

public class ChooseGameActivity extends SingleFragmentActivity {


    // выбор игры, обратите внимание, от какого она класса наследуется
    // смотрите этот класс

    @Override
    protected Fragment createFragment() {
        return new ChooseGameRecyclerFragment();
    }

}
