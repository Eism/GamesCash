package ru.team.ae3.gamescash;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

public class WelcomeActivity extends AppCompatActivity {


    // отсюда всё начинается

    private static int SPLASH_TIME_OUT = 2000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent mainInt = new Intent(WelcomeActivity.this, MainActivity.class);
                startActivity(mainInt);
                finish();
            }
        }, SPLASH_TIME_OUT);
    }


}
