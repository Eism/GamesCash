package ru.team.ae3.gamescash

import android.support.v4.app.FragmentActivity
import android.support.v4.app.FragmentManager
import android.support.v7.widget.PopupMenu
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView

/**
 * Created by ELNUR on 12.03.2017.
 */
class BankAdapter : RecyclerView.Adapter<BankAdapter.BankAdapterViewHolder>(), RateDialogFragment.Listener {

    companion object {
        private val DIALOG_DATE = "DialogDate"
    }

    var data = ArrayList<Player>()
    var selectionPlayer: Player? = null
    var view: View? = null
    override fun onBindViewHolder(holder: BankAdapter.BankAdapterViewHolder?, position: Int) {
        holder!!.playerName.text = data[position].name
        holder.playerBalance.text = data[position].balance.toString()
        holder.buttonSendMany.setOnClickListener {
            showPopupMenu(holder.buttonSendMany, position)
        }
        holder.itemView.setOnClickListener {
            showPopupMenu(holder.buttonSendMany, position)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): BankAdapter.BankAdapterViewHolder {
        val v: View = LayoutInflater.from(parent!!.context).inflate(R.layout.player_item_from_client, parent, false)
        return BankAdapterViewHolder(v)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    inner class BankAdapterViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        init {
            view = v
        }

        var playerName = view!!.findViewById(R.id.player_name) as TextView
        var playerBalance = view!!.findViewById(R.id.player_balance) as TextView
        var buttonSendMany = view!!.findViewById(R.id.button_send_many) as Button
    }

    override fun returnData(result: Int) {
        (data.first().mService as ServerService).sendMoney(selectionPlayer!!, result)
    }

    private fun showPopupMenu(v: View, position: Int) {
        val popupMenu = PopupMenu(view!!.context, v)
        popupMenu.inflate(R.menu.popupmenu)

        popupMenu
                .setOnMenuItemClickListener { item ->
                    when (item.itemId) {
                        R.id.send_many -> {
                            val manager: FragmentManager? = (view!!.context as FragmentActivity).supportFragmentManager

                            val dialog = RateDialogFragment.newInstance(Int.MAX_VALUE)
                            dialog.setListener(this@BankAdapter)
                            dialog.show(manager, DIALOG_DATE)

                            selectionPlayer = data[position]

                            true
                        }
                        else -> false
                    }
                }

        popupMenu.setOnDismissListener {
        }
        popupMenu.show()
    }

}