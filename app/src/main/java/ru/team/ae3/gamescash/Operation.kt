package ru.team.ae3.gamescash

import java.util.*

/**
 * Created by darke on 19.03.2017.
 */
class Operation(var to: Player, var from: Player, var count: Int)