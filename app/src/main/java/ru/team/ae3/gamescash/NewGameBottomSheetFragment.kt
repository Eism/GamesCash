package ru.team.ae3.gamescash

import android.app.Dialog
import android.os.Bundle
import android.support.design.widget.BottomSheetDialogFragment
import android.content.Intent
import android.view.View

/**
 * Created by darke on 18.03.2017.
 */

class NewGameBottomSheetFragment : BottomSheetDialogFragment(){
    override fun setupDialog(dialog: Dialog?, style: Int) {
        super.setupDialog(dialog, style)
        val contentView = View.inflate(context, R.layout.fragment_bottom_sheet, null)

        (contentView?.findViewById(R.id.fragment_host_menu))?.setOnClickListener {
            val intent = Intent(context, PrepareGameActivity::class.java)
            intent.putExtra(PrepareGameActivity.IS_SERVER,true)
            startActivity(intent)
        }
        (contentView?.findViewById(R.id.fragment_join_menu))?.setOnClickListener {
            val intent = Intent(context, PrepareGameActivity::class.java)
            intent.putExtra(PrepareGameActivity.IS_SERVER,false)
            startActivity(intent)
        }

        dialog?.setContentView(contentView)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return super.onCreateDialog(savedInstanceState)
    }
}