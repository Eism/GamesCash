package ru.team.ae3.gamescash

import android.app.AlertDialog
import android.content.*
import android.graphics.drawable.Drawable
import android.os.*
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.view.ViewPager
import android.support.v7.app.ActionBarActivity
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import java.util.ArrayList



 class GameActivity :ActionBarActivity() {


     companion object {
         val IS_SERVER = "is server"
     }
     var fragmentBank: BankContentFragment? = null
     var fragmentPlayer: PlayerContentFragment? = null
     var fragmentOperation: OperationFragment? = null

     var data : ArrayList<Player>? = null

     var mServiceMessenger: Messenger? = null
     var mServerBound = false
     var mMesseger = Messenger(IncomingHandler())
     var mService : AbstractService? = null

     private val mServerConnection = object : ServiceConnection {

         override fun onServiceConnected(className: ComponentName,
                                         service: IBinder) {
             // We've bound to LocalService, cast the IBinder and get LocalService instance
             mServiceMessenger = Messenger(service)
             mServerBound = true
             try {
                 var msg = Message()
                 msg.what = AbstractService.EActivityMessages.MSG_REGISTER_CLIENT.ordinal
                 msg.replyTo = mMesseger
                 mServiceMessenger!!.send(msg)

                 msg = Message()
                 msg.what = AbstractService.EActivityMessages.MSG_SERVICE.ordinal
                 mServiceMessenger!!.send(msg)
             } catch (e: RemoteException) {
                 Log.i("Client Msg", e.toString())
             }
         }

         override fun onServiceDisconnected(arg0: ComponentName) {
             mServerBound = false
         }
     }
     var bIsServer = true

     var toolbar: Toolbar? = null

     override fun onCreate(savedInstanceState: Bundle?) {
         super.onCreate(savedInstanceState)
         setContentView(R.layout.activity_action_game)

         toolbar = findViewById(R.id.tool_bar) as Toolbar
         setSupportActionBar(toolbar)
         toolbar!!.setTitleTextColor(resources.getColor(R.color.ms_white))
         val upArrow : Drawable = resources.getDrawable(R.drawable.ic_arrow_back_white_24dp)
         supportActionBar!!.setHomeAsUpIndicator(upArrow)

         fragmentOperation = OperationFragment()

         bIsServer = intent.getBooleanExtra(IS_SERVER, true)

         val viewPager = findViewById(R.id.viewPager) as ViewPager
         setupViewPager(viewPager)

         val tabLayout = findViewById(R.id.tabs) as TabLayout

         val appBar = findViewById(R.id.appbar)

         tabLayout.addTab(tabLayout.newTab().setText("Player"))
         if (bIsServer) {
             tabLayout.addTab(tabLayout.newTab().setText("Bank"))
         }

         if (bIsServer) {
             val serverIntent = Intent(applicationContext, ServerService::class.java)
             bindService(serverIntent, mServerConnection, Context.BIND_AUTO_CREATE)
         } else {
             val serverIntent = Intent(applicationContext, ClientService::class.java)
             bindService(serverIntent, mServerConnection, Context.BIND_AUTO_CREATE)
             appBar!!.visibility = View.GONE
         }

         tabLayout.setupWithViewPager(viewPager)

         println(tabLayout.selectedTabPosition)

         supportActionBar?.setDisplayHomeAsUpEnabled(true)
     }

     override fun onCreateOptionsMenu(menu: Menu): Boolean {
         // Inflate the menu; this adds items to the action bar if it is present.
         menuInflater.inflate(R.menu.menu_game, menu)
         return true
     }

     override fun onOptionsItemSelected(item: MenuItem): Boolean {
         // Handle action bar item clicks here. The action bar will
         // automatically handle clicks on the Home/Up button, so long
         // as you specify a parent activity in AndroidManifest.xml.
         val id = item.itemId
         if (id == R.id.action_history) {
            fragmentOperation!!.show(supportFragmentManager,"Fragment Operation")
            return true
         }

         return super.onOptionsItemSelected(item)
     }
     private fun setupViewPager(viewPager: ViewPager) {
         val adapter = MyFragmentPagerAdapter(supportFragmentManager)
         fragmentPlayer = PlayerContentFragment()
         adapter.addFragment(fragmentPlayer!!, "Player")
         if (bIsServer) {
             fragmentBank = BankContentFragment()
             adapter.addFragment(fragmentBank!!, "Bank")
         }
         viewPager.adapter = adapter
     }

     internal class MyFragmentPagerAdapter(manager: FragmentManager) : FragmentPagerAdapter(manager) {
         private val mFragmentList = ArrayList<Fragment>()
         private val mFragmentTitleList = ArrayList<String>()

         override fun getItem(position: Int): Fragment {
             return mFragmentList[position]
         }

         override fun getCount(): Int {
             return mFragmentList.size
         }

         fun addFragment(fragment: Fragment, title: String) {
             mFragmentList.add(fragment)
             mFragmentTitleList.add(title)
         }

         override fun getPageTitle(position: Int): CharSequence {
             return mFragmentTitleList[position]
         }
     }

     internal inner class IncomingHandler : Handler() {
         override fun handleMessage(msg: Message) {
             when (msg.what) {
                 AbstractService.EServiceMessages.MSG_ARE_GETTING_SERVICE.ordinal -> {
                     data = (msg.obj as AbstractService).clientList
                     if (bIsServer) {
                         mService = (msg.obj as ServerService)
                     }else {
                         mService = (msg.obj as ClientService)
                     }

                     if (fragmentPlayer != null) {
                         fragmentPlayer?.adapter?.data = data!!
                         fragmentPlayer?.adapter?.notifyDataSetChanged()
                         fragmentPlayer!!.headerPlayerName!!.text = data!!.first().name
                         fragmentPlayer!!.headerPlayerBalance!!.text = data!!.first().balance.toString()
                         fragmentBank?.adapter?.notifyDataSetChanged()
                     }
                     if (fragmentBank != null) {
                         fragmentBank?.adapter?.data = data!!
                     }

                     if (fragmentOperation != null){
                         val dataOperation = (msg.obj as AbstractService).operationList
                         fragmentOperation!!.adapter!!.data = dataOperation
                         fragmentOperation!!.adapter!!.notifyDataSetChanged()
                     }

                 }
                 AbstractService.EServiceMessages.MSG_END_GAME.ordinal -> {
                     onBackPressed()
                 }
                 AbstractService.EServiceMessages.MSG_DATA_CHANGED.ordinal -> {
                     if(data!!.filter { !it.bIsAccepted && bIsServer }.isNotEmpty()) {
                         showAcceptedClient(data!!.filter { !it.bIsAccepted }.last())
                     }
                     if (fragmentPlayer != null) {
                         fragmentPlayer!!.adapter.notifyDataSetChanged()
                     }
                     if (fragmentBank != null) {
                         fragmentBank!!.adapter.notifyDataSetChanged()
                     }
                     if (fragmentOperation != null){
                         fragmentOperation!!.adapter.notifyDataSetChanged()
                     }
                 }
                 else -> super.handleMessage(msg)
             }
         }
     }

     fun showAcceptedClient(player : Player){

         val builder = AlertDialog.Builder(this)

         builder.setTitle("Новый игрок "+player.name)
                 .setMessage("Хотите принять?")
                 .setCancelable(false)
                 .setPositiveButton("Yes") { dialog, which ->
                     run {
                         player.acceptClient(true)
                     }
                 }
                 .setNegativeButton("No") { dialog, which ->
                     run {
                         player.acceptClient(false)
                         dialog!!.cancel()
                     }
                 }
         val alert = builder.create()
         alert.show()
     }

     override fun onBackPressed() {
         val builder = AlertDialog.Builder(this)

         builder.setMessage("Are you sure you want to exit?")
                 .setCancelable(false)
                 .setPositiveButton("Yes") { dialog, which ->
                     run {
                         val msg = Message()
                         msg.what = AbstractService.EActivityMessages.MSG_STOP_SERVICE.ordinal
                         mServiceMessenger!!.send(msg)

                         val intent = Intent(applicationContext,MainActivity::class.java)
                         startActivity(intent)

                     }
                 }
                 .setNegativeButton("No") { dialog, which -> dialog!!.cancel() }
         val alert = builder.create()
         alert.show()
     }

     override fun onDestroy() {
         unbindService(mServerConnection)
         super.onDestroy()
     }
 }
