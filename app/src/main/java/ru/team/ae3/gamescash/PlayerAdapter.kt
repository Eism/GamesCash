package ru.team.ae3.gamescash

import android.support.v4.app.FragmentActivity
import android.support.v4.app.FragmentManager
import android.support.v7.widget.PopupMenu
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView



/**
 * Created by ELNUR on 12.03.2017.
 */
class PlayerAdapter : RecyclerView.Adapter<PlayerAdapter.PlayerAdapterViewHolder>(), RateDialogFragment.Listener {

    companion object {

        val DIALOG_DATE = "DialogDate"

        private val REQUEST_BALANCE = 0
    }

    var data = ArrayList<Player>()

    var selectionPlayer: Player? = null
    var bIsBank:Boolean = true

    var view: View? = null

    override fun onBindViewHolder(holder: PlayerAdapter.PlayerAdapterViewHolder?, position: Int) = if (position == 0) {
        holder!!.playerName.text = "Bank"
        holder.playerBalance.text = ""

        view!!.findViewById(R.id.imageView2).visibility = View.VISIBLE
        holder.buttonOperation.setOnClickListener {
            showPopupMenu(holder.buttonOperation, position)
        }
        holder.itemView.setOnClickListener {
            showPopupMenu(holder.buttonOperation, position)
        }
    }else {
        holder!!.playerName.text = data[position].name
        holder.playerBalance.text = data[position].balance.toString()

        view!!.findViewById(R.id.imageView2).visibility = View.GONE

        holder.buttonOperation.setOnClickListener {
            showPopupMenu(holder.buttonOperation, position)
        }
        holder.itemView.setOnClickListener {
            showPopupMenu(holder.buttonOperation, position)
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): PlayerAdapter.PlayerAdapterViewHolder {
        val v :View = LayoutInflater.from(parent!!.context).inflate(R.layout.player_item_from_client, parent, false)
        return PlayerAdapterViewHolder(v)
    }

    override fun getItemCount(): Int {
        return data.size
    }


    inner class PlayerAdapterViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        init {
            view = v
        }
        var playerName = view!!.findViewById(R.id.player_name) as TextView
        var playerBalance = view!!.findViewById(R.id.player_balance) as TextView
        var buttonOperation = view!!.findViewById(R.id.button_send_many) as Button

    }
    override fun returnData(result : Int) {
        if (bIsBank)
            data.first().requestSendMoneyToBank(result)
        else data.first().requestSendMoney(selectionPlayer!!,result)
    }


    private fun showPopupMenu(v: View, position: Int) {
        val popupMenu = PopupMenu(view!!.context, v)
        popupMenu.inflate(R.menu.popupmenu)

        popupMenu
                .setOnMenuItemClickListener { item ->
                    when (item.itemId) {

                        R.id.send_many -> {
                            if (position == 0) {
                                val manager: FragmentManager? = (view!!.context as FragmentActivity).supportFragmentManager
                                val dialog = RateDialogFragment.newInstance(data.first().balance)

                                dialog.setListener(this@PlayerAdapter)
                                dialog.show(manager, DIALOG_DATE)

                                bIsBank = true
                            }else {
                                val manager: FragmentManager? = (view!!.context as FragmentActivity).supportFragmentManager
                                val dialog = RateDialogFragment.newInstance(data.first().balance)

                                dialog.setListener(this@PlayerAdapter)
                                dialog.show(manager, DIALOG_DATE)

                                selectionPlayer = data[position]
                                bIsBank = false
                            }
                            true
                        }
                        else -> false
                    }
                }

        popupMenu.setOnDismissListener {
        }
        popupMenu.show()
    }

}