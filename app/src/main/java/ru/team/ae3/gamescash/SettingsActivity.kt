package ru.team.ae3.gamescash

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View

import at.markushi.ui.CircleButton


class SettingsActivity : AppCompatActivity() {

    private var but_next: CircleButton? = null


    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)


        but_next = findViewById(R.id.circleButton) as CircleButton
        but_next!!.setOnClickListener {
            val intent = Intent(applicationContext, LobbyActivity::class.java)
            intent.putExtra(LobbyActivity.MODE, true)
            startActivity(intent)
        }

    }

    companion object {


        val SELECTED_GAME = "mygame"
    }

}
