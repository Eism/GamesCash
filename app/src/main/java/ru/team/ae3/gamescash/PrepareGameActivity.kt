package ru.team.ae3.gamescash


import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.stepstone.stepper.Step
import com.stepstone.stepper.StepperLayout
import com.stepstone.stepper.adapter.AbstractFragmentStepAdapter
import com.stepstone.stepper.viewmodel.StepViewModel
import com.stepstone.stepper.VerificationError



class PrepareGameActivity : AppCompatActivity(), StepperLayout.StepperListener, ChooseGameSelect {


    companion object{
        val PERSON_NAME = "personName"
        val IS_SERVER = "server or client"
    }

    var setNameFragment : SetNameFragment? = null
    var chooseGameRecyclerFragment : ChooseGameRecyclerFragment? = null
    var searchServersFragment : SearchServersActivity? = null
    var personName : String = ""
    var bIsServer = true

    override fun Done() {
        val intent = Intent(applicationContext, LobbyActivity::class.java)
        intent.putExtra(LobbyActivity.MODE, true)
        intent.putExtra(LobbyActivity.PERSON_NAME,personName)
        startActivity(intent)
    }

    override fun onCompleted(completeButton: View) {
    }

    override fun onError(verificationError: VerificationError) {
    }

    override fun onStepSelected(newStepPosition: Int) {
        when (newStepPosition){
            1 -> personName = setNameFragment!!.personName
        }
    }

    override fun onReturn() {
        finish()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
   //     if (savedInstanceState != null) personName = savedInstanceState.getString(PERSON_NAME)
        bIsServer = intent.getBooleanExtra(IS_SERVER, true)
        setContentView(R.layout.activity_prepare_game)
        val stepperLayout = findViewById(R.id.stepper_layout) as StepperLayout
        stepperLayout.setListener(this)
        stepperLayout.adapter = PrepareStepperAdapter(supportFragmentManager, this)
    }



    override fun onDestroy() {
        var bundle = Bundle()
        bundle.putString(PERSON_NAME,personName)
  //      onSaveInstanceState(bundle)
        super.onDestroy()
    }

    inner class PrepareStepperAdapter(fragmentManager: android.support.v4.app.FragmentManager, context: Context) :
            AbstractFragmentStepAdapter(fragmentManager, context) {
        override fun getCount(): Int {
            return 2
        }

        override fun createStep(position: Int): Step {
            when(position){
                0 -> {
                    setNameFragment = SetNameFragment()
                    return setNameFragment!!
                }
                1 -> {
                    if(bIsServer) {
                        chooseGameRecyclerFragment = ChooseGameRecyclerFragment()
                        return chooseGameRecyclerFragment!!
                    }else
                    {
                        searchServersFragment = SearchServersActivity()
                        return  searchServersFragment!!
                    }
                }
                else -> {return ChooseGameRecyclerFragment()}
            }

        }

        override fun getViewModel(position: Int): StepViewModel {
            when (position){
                0 -> {
                    return StepViewModel.Builder(context)
                            .create()
                }
                1 -> {
                    return StepViewModel.Builder(context)
                            .create()
                }
                else -> return StepViewModel.Builder(context).create()
            }
        }
    }
}
