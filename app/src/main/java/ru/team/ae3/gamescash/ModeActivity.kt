package ru.team.ae3.gamescash

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Button

class ModeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)


        (findViewById(R.id.host_b) as Button).setOnClickListener {
            val intent = Intent(this@ModeActivity, ChooseGameActivity::class.java)
            startActivity(intent)
        }



        (findViewById(R.id.join_b) as Button).setOnClickListener {
            val intent = Intent(applicationContext, SearchServersActivity::class.java)
            startActivity(intent)
        }

    }


    override fun onBackPressed() {
        super.onBackPressed()
        val intent = Intent(this@ModeActivity, MainActivity::class.java)
        startActivity(intent)
    }


}


