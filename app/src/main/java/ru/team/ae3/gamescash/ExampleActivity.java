//package ru.team.ae3.gamescash;
//
//
//import android.graphics.Color;
//import android.os.Bundle;
//import android.support.v7.app.AppCompatActivity;
//import android.view.View;
//import android.widget.TextView;
//
//import com.diegodobelo.expandingview.ExpandingItem;
//import com.diegodobelo.expandingview.ExpandingList;
//
//public class ExampleActivity extends AppCompatActivity {
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.example_activity);
//
//
//        ExpandingList expandingList = (ExpandingList) findViewById(R.id.expanding_list_main);
//
//        ExpandingItem item = expandingList.createNewItem(R.layout.example_activty_item);
//        ((TextView) item.findViewById(R.id.title)).setText("It works!!!");
//
//
//        item.createSubItems(3);
//
//
//        View subItemZero = item.getSubItemView(0);
//        ((TextView) subItemZero.findViewById(R.id.sub_title)).setText("Cool");
//
//        View subItemOne = item.getSubItemView(1);
//        ((TextView) subItemOne.findViewById(R.id.sub_title)).setText("Awesome");
//
//
//
//        item.setIndicatorColorRes(R.color.colorPrimary);
//        item.setIndicatorIconRes(R.drawable.ic_action_tick);
//
//    }
//
//}
