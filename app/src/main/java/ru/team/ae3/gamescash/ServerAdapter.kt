package ru.team.ae3.gamescash

import android.app.ProgressDialog
import android.content.*
import android.os.Binder
import android.os.Handler
import android.os.IBinder
import android.os.Message
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import java.util.*
import android.os.Messenger

/**
 * Created by Ericos on 02.02.2017.
 */
class ServerAdapter : RecyclerView.Adapter<ServerAdapter.TaskAdapterViewHolder>() {

    companion object{
        val PREFS_NAME = "GameCashPrefsFile"
        val PERSON_NAME = "personName"
    }
    var v: View? = null

    var data = ArrayList<Server>()

    var mServerBound = false

    var pD: ProgressDialog? = null


    val mMessenger = Messenger(IncomingHandler())

    var mServiceBound = false

    private val mClientConnection = object : ServiceConnection {

        override fun onServiceConnected(className: ComponentName,
                                        service: IBinder) {

            mServerBound = true
            var mService = Messenger(service)

//            val msg = Message()
//            msg.what = ClientService.MSG_REGISTER_CLIENT
//            mServiceMessenger.send(msg)
//            Log.i("Client Msg","Send MSG_REGISTER_CLIENT")
/*
            //var msg = Message()
            //msg.obj = mServerService!!.clientList.first().bIsAccepted.toString()
            //mHandlerServer!!.sendMessage(msg)*/
        }

        override fun onServiceDisconnected(p0: ComponentName?) {

        }
    }

    override fun onDetachedFromRecyclerView(recyclerView: RecyclerView?) {
        super.onDetachedFromRecyclerView(recyclerView)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TaskAdapterViewHolder {
        v = LayoutInflater.from(parent.context).inflate(R.layout.item_server, parent, false)
        return TaskAdapterViewHolder(v!!)
    }

    override fun onBindViewHolder(holder: TaskAdapterViewHolder, position: Int) {
        holder.view.setOnClickListener {
            if (!mServiceBound) {
                pD = object : ProgressDialog(holder.view.context){
                    override fun onStop() {
                        super.onStop()
                        context.unbindService(mClientConnection)
                    }
                }
                pD!!.setTitle("Подключение")
                pD!!.setMessage("Проверка сервера")
                pD!!.show()

                val intent = Intent(holder.view.context, ClientService::class.java)
                intent.putExtra(ClientService.TCP_ADDRESS, data[position].address)
                intent.putExtra(ClientService.MESSAGER, mMessenger)
                intent.putExtra(ClientService.PLAYER_NAME,(holder.view.context as PrepareGameActivity).personName)
                pD!!.context.startService(intent)
                pD!!.context.bindService(intent, mClientConnection, Context.BIND_AUTO_CREATE)
                mServiceBound = true
            }

        }
        holder.nameText.text = (data[position].name + " room")


    }

    class TaskAdapterViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        var view: View
        var nameText: TextView

        init {
            view = v
            nameText = view.findViewById(R.id.name_server) as TextView

        }
    }

    var bIsAccepted = false
    var bGameIsLobby = false
    var bGameIsStarted = false

    internal inner class IncomingHandler : Handler() {
        override fun handleMessage(msg: Message) {
            when (msg.what) {
                AbstractService.EServiceMessages.MSG_IS_ACCEPTED.ordinal -> {
                    bIsAccepted = true
                    nextActivity()
                }
                AbstractService.EServiceMessages.MSG_IS_NOT_START_GAME.ordinal -> {
                    bGameIsLobby = true
                    nextActivity()
                }
                AbstractService.EServiceMessages.MSG_START_GAME.ordinal -> {
                    bGameIsStarted = true
                    nextActivity()
                }
                else -> super.handleMessage(msg)
            }
        }
    }

    fun nextActivity(){
        if(bIsAccepted){
            when {
                bGameIsLobby -> {
                    var intent = Intent(v!!.context, LobbyActivity::class.java)
                    intent.putExtra(LobbyActivity.MODE, false)
                    intent.putExtra(LobbyActivity.PERSON_NAME,"")
                    v!!.context.startActivity(intent)
                    pD!!.dismiss()
                }
                bGameIsStarted -> {
                    var intent = Intent(v!!.context, GameActivity::class.java)
                    intent.putExtra(GameActivity.IS_SERVER,false)
                    v!!.context.startActivity(intent)
                    pD!!.dismiss()
                }
            }
        }

    }
}