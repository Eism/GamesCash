package ru.team.ae3.gamescash

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.PorterDuff
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.os.*
import android.support.design.widget.FloatingActionButton
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

class LobbyRecyclerFragment : Fragment() {

    var mServerBound = false
    var list : RecyclerView? = null
    var adapter = ClientAdapter()

    var mService: Messenger? = null
    var mMesseger = Messenger(IncomingHandler())

    var mClientBound = false

    private var recyclerView: RecyclerView? = null

    private val mServerConnection = object : ServiceConnection {

        override fun onServiceConnected(className: ComponentName,
                                        service: IBinder) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            mService = Messenger(service)
            mServerBound = true
            try {
                var msg = Message()
                msg.what = AbstractService.EActivityMessages.MSG_REGISTER_CLIENT.ordinal
                msg.replyTo = mMesseger
                mService!!.send(msg)

                msg = Message()
                msg.what = AbstractService.EActivityMessages.MSG_SERVICE.ordinal
                mService!!.send(msg)
            } catch (e: RemoteException) {
                Log.i("Client Msg", e.toString())
            }


        }

        override fun onServiceDisconnected(arg0: ComponentName) {
            mServerBound = false
        }
    }

    private val mClientConnection = object : ServiceConnection {

        override fun onServiceConnected(className: ComponentName, service: IBinder) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            mService = Messenger(service)
            mClientBound = true
            try {
                var msg = Message()
                msg.what = AbstractService.EActivityMessages.MSG_REGISTER_CLIENT.ordinal
                msg.replyTo = mMesseger
                mService!!.send(msg)

                msg = Message()
                msg.what = AbstractService.EActivityMessages.MSG_SERVICE.ordinal
                mService!!.send(msg)
            } catch (e: RemoteException) {
                Log.i("Client Msg", e.toString())
            }
        }

        override fun onServiceDisconnected(arg0: ComponentName) {
            mServerBound = false
        }
    }

    override fun onDetach() {
        Log.i("LobbyRecyclerFragment", "Detach")
        if(!(activity as LobbyActivity).bIsServer) {
            activity.unbindService(mClientConnection)
        }else{
            activity.unbindService(mServerConnection)
        }
        super.onDetach()
    }

    override fun onDestroy() {

        super.onDestroy()
        Log.i("LobbyRecyclerFragment", "Destroy")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)

        retainInstance = true
    }


    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view = inflater!!.inflate(R.layout.activity_lobby, container, false)

        list = view.findViewById(R.id.client_list) as RecyclerView
        list!!.layoutManager = LinearLayoutManager(context)
        adapter.bIsServer = (activity as LobbyActivity).bIsServer
        list!!.adapter = adapter
        setUpItemTouchHelper()
        setUpAnimationDecoratorHelper()

        var btCircle = view.findViewById(R.id.but_next_lobby) as FloatingActionButton

        if (!(activity as LobbyActivity).bIsServer ) btCircle.visibility = View.GONE

        if ((activity as LobbyActivity).bIsServer) {
            val serverIntent = Intent(context, ServerService::class.java)
            activity.startService(serverIntent)
            activity.bindService(serverIntent, mServerConnection, Context.BIND_AUTO_CREATE)
        }else{
            val clientIntent = Intent(context, ClientService::class.java)
            clientIntent.putExtra(ClientService.PLAYER_NAME,(activity as LobbyActivity).personName)
            activity.bindService(clientIntent, mClientConnection, Context.BIND_AUTO_CREATE)
        }

        (view.findViewById(R.id.but_next_lobby) as FloatingActionButton).setOnClickListener{
            if ((activity as LobbyActivity).bIsServer){
                var msg = Message()
                msg.what = AbstractService.EActivityMessages.MSG_START_GAME.ordinal
                mService!!.send(msg)
            }
        }

        return view
    }


    // это поиск, может пригодится
    /*
    override fun onCreateOptionsMenu(menu: Menu?, menuInflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, menuInflater)
        menuInflater!!.inflate(R.menu.fragment_search, menu)
    }
    */



    internal inner class IncomingHandler : Handler() {
        override fun handleMessage(msg: Message) {
            when (msg.what) {
                AbstractService.EServiceMessages.MSG_ARE_GETTING_SERVICE.ordinal -> {
                    adapter.data = (msg.obj as AbstractService).clientList
                    if((activity as LobbyActivity).bIsServer) adapter.data.first().name = (activity as LobbyActivity).personName
                    adapter.notifyDataSetChanged()
                }
                AbstractService.EServiceMessages.MSG_END_GAME.ordinal ->{
                    activity.onBackPressed()
                }
                AbstractService.EServiceMessages.MSG_DATA_CHANGED.ordinal ->{
                    adapter.notifyDataSetChanged()
                }
                AbstractService.EServiceMessages.MSG_START_GAME.ordinal -> {
                    val intent = Intent(context,GameActivity::class.java)
                    intent.putExtra(GameActivity.IS_SERVER,(activity as LobbyActivity).bIsServer)
                    startActivity(intent)
                }
                else -> super.handleMessage(msg)
            }
        }
    }





    // методы для recycler
    private fun setUpItemTouchHelper() {

        val simpleItemTouchCallback = object : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {


            // not important, we don't want drag & drop
            override fun onMove(recyclerView: RecyclerView?, viewHolder: RecyclerView.ViewHolder?, target: RecyclerView.ViewHolder?): Boolean {
                return false
            }

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder?, direction: Int) {

                if (viewHolder!!.position == 0){
                    return
                }

                val swipedPosition = viewHolder!!.adapterPosition
                val adapter = list!!.adapter as ClientAdapter

                adapter.remove(swipedPosition)
            }


            internal var background: Drawable? = null
            internal var xMark: Drawable? = null
            internal var xMarkMargin: Int = 0
            internal var initiated: Boolean = false

            private fun init() {
                background = ColorDrawable(Color.RED)
                xMark = ContextCompat.getDrawable(context, R.drawable.ic_clear_24dp)
                xMark!!.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP)
                xMarkMargin = context.resources.getDimension(R.dimen.ic_clear_margin).toInt()
                initiated = true
            }


            override fun getSwipeDirs(recyclerView: RecyclerView?, viewHolder: RecyclerView.ViewHolder?): Int {
                val position = viewHolder!!.adapterPosition
                val clientAdapter = recyclerView!!.adapter as ClientAdapter

                return super.getSwipeDirs(recyclerView, viewHolder)
            }


            override fun onChildDraw(c: Canvas, recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, dX: Float, dY: Float, actionState: Int, isCurrentlyActive: Boolean) {
                val itemView = viewHolder.itemView

                if (viewHolder.position == 0) {
                    return
                }

                // not sure why, but this method get's called for viewholder that are already swiped away
                if (viewHolder.adapterPosition == -1) {
                    // not interested in those
                    return
                }

                if (!initiated) {
                    init()
                }


                // draw red background
                background!!.setBounds(itemView.right + dX.toInt(), itemView.top, itemView.right, itemView.bottom)
                background!!.draw(c)

                // draw x mark
                val itemHeight = itemView.bottom - itemView.top
                val intrinsicWidth = xMark!!.intrinsicWidth
                val intrinsicHeight = xMark!!.intrinsicWidth

                val xMarkLeft = itemView.right - xMarkMargin - intrinsicWidth
                val xMarkRight = itemView.right - xMarkMargin
                val xMarkTop = itemView.top + (itemHeight - intrinsicHeight) / 2
                val xMarkBottom = xMarkTop + intrinsicHeight
                xMark!!.setBounds(xMarkLeft, xMarkTop, xMarkRight, xMarkBottom)

                xMark!!.draw(c)


                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
            }



        }

        val mItemTouchHelper = ItemTouchHelper(simpleItemTouchCallback)
        mItemTouchHelper.attachToRecyclerView(list)
    }



    private fun setUpAnimationDecoratorHelper() {
        list!!.addItemDecoration(object : RecyclerView.ItemDecoration() {

            // we want to cache this and not allocate anything repeatedly in the onDraw method
            internal var background: Drawable? = null
            internal var initiated: Boolean = false

            private fun init() {
                background = ColorDrawable(Color.RED)
                initiated = true
            }


            override fun onDraw(c: Canvas, parent: RecyclerView, state: RecyclerView.State?) {
                super.onDraw(c, parent, state)

            }
        })

    }




}
