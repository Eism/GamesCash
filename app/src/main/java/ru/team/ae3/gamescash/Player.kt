package ru.team.ae3.gamescash

import android.app.Service
import android.os.Message
import android.os.RemoteException
import org.json.JSONObject
import java.io.*
import java.net.*
import java.util.*
import kotlin.concurrent.timerTask

/**
 * Created by Ericos on 07.03.2017.
 */

class Player(role : ERole, service : AbstractService) {
    companion object{
        val BANK_UUID = "00000000-0000-0000-C000-000000000046"
    }
    var uuid: UUID = UUID.randomUUID()
    var name: String = ""
    var balance: Int = 0

    enum class ERole {
        ROLE_Authority,
        ROLE_AutonomousProxy,
        ROLE_SimulatedProxy,
        ROLE_Bank
    }

    private var mSocket: Socket? = null
    private var mBufferOut: PrintWriter? = null
    private var mBufferIn: BufferedReader? = null
    private var timer: Timer = Timer()

    var mRun = true
    var bIsAccepted = false
    var Role = role
    var mService = service

    init {
        when (Role) {
            ERole.ROLE_Authority -> {

                val sService = mService as ServerService
                sService.clientList.add(this)
                if (mSocket != null) {
                    mService.onDataChanged()
                }
            }
            ERole.ROLE_SimulatedProxy -> {
                val sService = mService as ClientService
                sService.clientList.add(this)
                mService.onDataChanged()
            }
            ERole.ROLE_AutonomousProxy -> {
                /*
                    in ClientService Thread
                */
            }
            ERole.ROLE_Bank -> {
                name = "Bank"
                uuid = UUID.fromString(BANK_UUID)
            }
        }
    }

    constructor(role: ERole, service: AbstractService, socket: Socket, name: String) : this(role, service, socket) {
        this.name = name
    }

    constructor(role: ERole, service: AbstractService, socket: Socket) : this(role, service) {
        this.mSocket = socket
        this.mBufferOut = PrintWriter(BufferedWriter(OutputStreamWriter(socket.getOutputStream())))
        this.mBufferIn = BufferedReader(InputStreamReader(socket.getInputStream()))
        when (Role) {
            ERole.ROLE_Authority -> {
                Thread(Runnable {
                    checkClient()
                    while (mRun) {
                        try {
                            if (mSocket!!.isConnected && mSocket!!.isBound && !mSocket!!.isInputShutdown) {
                                var message = mBufferIn!!.readLine()
                                if (message != null) {
                                    when {
                                        message.contains("Name: ") -> {
                                            this.name = message.subSequence(6, message.length).toString()
                                            mService.onDataChanged()
                                        }
                                        message.contains("Request: get players") -> sendAllPlayers()
                                        message.contains("Request: send money") -> {
                                            message = message.replace("Request: send money ", "")
                                            message = message.replace("To: ", "")
                                            message = message.replace("Count: ", "")
                                            val parseMessage = message.split(" ")
                                            val count = parseMessage[1].toInt()
                                            if (parseMessage[0] == "Bank") {
                                                sendToBankMoney(count)
                                            } else {
                                                val uuid = UUID.fromString(parseMessage[0])
                                                mService.clientList.filter { it.uuid == uuid }
                                                        .forEach { sendMoney(it, count) }
                                            }
                                            mService.onDataChanged()
                                        }
                                        message.contains("I went out") -> {
                                            disconnectClient()
                                            mService.onDataChanged()
                                        }
                                        message.contains("Request: game status") -> {
                                            if (mService.bGameIsStarted) sendMessage("Game is started")
                                            else sendMessage("Game is not started")
                                        }
                                        else -> sendMessage("Unknown command")
                                    }
                                }
                            } else {
                                disconnectClient()
                            }
                        } catch (e: SocketTimeoutException) {
                            if (mSocket!!.isClosed) {
                                disconnectClient()
                            }
                        } catch (e: SocketException) {
                            if (mSocket!!.isClosed) {
                                disconnectClient()
                            }
                        }
                    }
                }).start()

            }
            ERole.ROLE_SimulatedProxy -> {

            }
            ERole.ROLE_AutonomousProxy -> {
                Thread(Runnable {
                    mBufferOut = PrintWriter(BufferedWriter(OutputStreamWriter(mSocket!!.getOutputStream())))
                    mBufferIn = BufferedReader(InputStreamReader(mSocket!!.getInputStream()))
                    mRun = true
                    while (mRun) {
                        try {
                            var message = mBufferIn!!.readLine()
                            if (message != null) {
                                when {
                                    message == "You are valid?" -> {
                                        sendMessage("Valid")
                                    }
                                    message.contains("Ok: ") -> {
                                        uuid = UUID.fromString(message.subSequence(4, message.length).toString())
                                        sendMessage("Name: " + this.name)
                                        mService.onDataChanged()
                                        sendMessage("Request: game status")
                                    }
                                    message == "Accepted" -> {
                                        bIsAccepted = true
                                        sendMessage("Request: get players")
                                        val msg = Message()
                                        msg.what = AbstractService.EServiceMessages.MSG_IS_ACCEPTED.ordinal
                                        try {
                                            service.mActivity!!.send(msg)
                                        } catch (e: RemoteException) {
                                            e.printStackTrace()
                                        }
                                    }
                                    message.contains("Rejected") -> {
                                        val msg = Message()
                                        msg.what = AbstractService.EServiceMessages.MSG_END_GAME.ordinal
                                        msg.obj = message
                                        try {
                                            service.mActivity!!.send(msg)
                                        } catch (e: RemoteException) {
                                            e.printStackTrace()
                                        }

                                    }
                                    message.contains("You kicked: ") -> {
                                        message = message.replace("You kicked: ", "")
                                        val msg = Message()
                                        msg.what = AbstractService.EServiceMessages.MSG_END_GAME.ordinal
                                        msg.obj = message
                                        try {
                                            service.mActivity!!.send(msg)
                                        } catch (e: RemoteException) {
                                            e.printStackTrace()
                                        }

                                    }
                                    message.contains("New player: ") -> {
                                        if (bIsAccepted) {
                                            message = message.replace("New player: ", "")
                                            message = message.replace("; ", "")
                                            val parseMessage = message.split(", ")
                                            val player = Player(ERole.ROLE_SimulatedProxy, mService, UUID.fromString(parseMessage[1]))
                                            player.name = parseMessage[0]
                                            player.balance = parseMessage[2].toInt()
                                            mService.onDataChanged()
                                        }
                                    }
                                    message.contains("Kick player: ") -> {
                                        message = message.replace("Kick player: ", "")
                                        message = message.replace("; ", "")
                                        val parseMessage = message.split(", ")
                                        mService.clientList
                                                .filter { it.name == parseMessage[0] && it.uuid == UUID.fromString(parseMessage[1]) }
                                                .forEach(Player::disconnectClient)
                                        mService.onDataChanged()
                                    }
                                    message.contains("Players: ") -> {
                                        message = message.replace("Players: ", "")

                                        val parseSequence = message.split("; ")
                                        parseSequence
                                                .filter { it != "" }
                                                .forEach {
                                                    val parseMessage = it.split(", ")
                                                    if (UUID.fromString(parseMessage[1]) != uuid) {
                                                        val player = Player(ERole.ROLE_SimulatedProxy, mService, UUID.fromString(parseMessage[1]))
                                                        player.name = parseMessage[0]
                                                        player.balance = parseMessage[2].toInt()
                                                    }
                                                }
                                        mService.onDataChanged()
                                    }
                                    message.contains("Operation: send money ") &&
                                            !(message.contains("From: Bank") || message.contains("To: Bank")) -> {
                                        message = message.replace("Operation: send money From: ", "")
                                        message = message.replace("To: ", "")
                                        message = message.replace("Count: ", "")
                                        val parseMessage = message.split(" ")
                                        val from = UUID.fromString(parseMessage[0])
                                        val to = UUID.fromString(parseMessage[1])
                                        val count = parseMessage[2].toInt()
                                        var toPlayer : Player? = null
                                        var fromPlayer : Player? = null
                                        mService.clientList.filter { it.uuid == to || it.uuid == from }
                                                .forEach {
                                                    when (it.uuid) {
                                                        from -> {
                                                            it.balance -= count
                                                            fromPlayer = it
                                                        }
                                                        to -> {
                                                            it.balance += count
                                                            toPlayer = it
                                                        }
                                                    }
                                                }
                                        mService.operationList.add(Operation(toPlayer!!,fromPlayer!!,count))
                                        mService.onDataChanged()
                                    }
                                    message.contains("Operation: send money ") &&
                                            (message.contains("From: Bank") || message.contains("To: Bank")) -> {
                                        message = message.replace("Operation: send money ", "")
                                        var fromBank: Boolean
                                        if (message.contains("From: Bank")) {
                                            message = message.replace("From: Bank ", "")
                                            message = message.replace("To: ", "")
                                            message = message.replace("Count: ", "")
                                            fromBank = true
                                        } else {
                                            message = message.replace("From: ", "")
                                            message = message.replace("To: Bank ", "")
                                            message = message.replace("Count: ", "")
                                            fromBank = false
                                        }
                                        val parseMessage = message.split(" ")
                                        val to = UUID.fromString(parseMessage[0])
                                        val count = parseMessage[1].toInt()
                                        mService.clientList.filter { it.uuid == to }
                                                .forEach {
                                                    when (it.uuid) {
                                                        to -> {
                                                            if (fromBank) {
                                                                it.balance += count
                                                                mService.operationList.add(Operation(it, mService.bank, count))
                                                            }
                                                            else {
                                                                it.balance -= count
                                                                mService.operationList.add(Operation(mService.bank, it, count))
                                                            }
                                                        }
                                                    }
                                                }
                                        mService.onDataChanged()
                                    }
                                    message == "Game is started" -> {
                                        val msg = Message()
                                        msg.what = AbstractService.EServiceMessages.MSG_START_GAME.ordinal
                                        service.mActivity!!.send(msg)
                                    }
                                    message == "Game is not started" -> {
                                        val msg = Message()
                                        msg.what = AbstractService.EServiceMessages.MSG_IS_NOT_START_GAME.ordinal
                                        service.mActivity!!.send(msg)
                                    }
                                }
                            }
                        } catch (e: SocketException) {

                        } catch (e: SocketTimeoutException) {
                            if (mSocket!!.isClosed) {
                                disconnectClient()
                            }
                        }
                    }
                }).start()
            }
        }
    }

    constructor(role: ERole, service: AbstractService, uuid: UUID) : this(role, service) {
        this.uuid = uuid
    }

    fun acceptClient(accept: Boolean) {
        if (Role == ERole.ROLE_Authority) {
            if (accept) {
                sendMessage("Accepted")
                bIsAccepted = true
                timer.cancel()
                (mService as ServerService).multicastSend(this, "New player: " +
                        this.name + ", " +
                        this.uuid + ", " +
                        this.balance.toString() + "; ", false)
                mService.onDataChanged()
            } else {
                sendMessage("Rejected")
                bIsAccepted = false
                timer.cancel()
                disconnectClient()
            }
        }
    }

    fun kickClient(message: String) {
        if (Role == ERole.ROLE_Authority && mSocket != null) {
            sendMessage("You kicked: " + message)
            timer.cancel()
            disconnectClient()
        }
    }

    fun sendMessage(message: String) {
        if (Role != ERole.ROLE_SimulatedProxy && mSocket != null) {
            if (!mBufferOut!!.checkError()) {
                mBufferOut!!.println(message)
                mBufferOut!!.flush()
            }
        }
    }

    fun requestSendMoney(player: Player, count: Int) {
        when (Role) {
            ERole.ROLE_Authority -> {
                sendMoney(player, count)
                mService.onDataChanged()
            }
            ERole.ROLE_AutonomousProxy -> {
                val message = "Request: send money " + "To: " + player.uuid + " Count: " + count.toString()
                sendMessage(message)
            }
            else -> {
            }
        }
    }

    fun requestSendMoneyToBank(count: Int) {
        when (Role) {
            ERole.ROLE_Authority -> {
                sendToBankMoney(count)
                mService.onDataChanged()
            }
            ERole.ROLE_AutonomousProxy -> {
                val message = "Request: send money " + "To: " + "Bank" + " Count: " + count.toString()
                sendMessage(message)
            }
            else -> {
            }
        }
    }

    private fun checkClient() {
        if (Role == ERole.ROLE_Authority) {
            sendMessage("You are valid?")
            mSocket!!.soTimeout = 10000
            try {
                var str = mBufferIn!!.readLine()
                if (str != "Valid") {
                    sendMessage("You invalid")
                    disconnectClient()
                } else {
                    sendMessage("Ok: " + uuid)
                    timer.schedule(timerTask {
                        acceptClient(false)
                    }, 10000)
                    mSocket!!.soTimeout = 500
                }
            } catch (e: SocketException) {
                sendMessage("You invalid")
                disconnectClient()
            }
        }
    }

    private fun disconnectClient() {
        mRun = false
        when (Role) {
            ERole.ROLE_Authority -> {
                var service = mService as ServerService
                service.clientList.remove(this)
                (mService as ServerService).multicastSend(this, "Kick player: " +
                        this.name + ", " +
                        this.uuid + "; ", false)
                mService.onDataChanged()
                try {
                    if (mSocket != null) {
                        mSocket!!.close()
                    }
                } catch (e: SocketException) {
                    e.printStackTrace()
                }
            }
            ERole.ROLE_SimulatedProxy -> {

            }
            ERole.ROLE_AutonomousProxy -> {
                sendMessage("I went out")
                try {
                    val msg = Message()
                    msg.what = AbstractService.EServiceMessages.MSG_END_GAME.ordinal
                    mService.mActivity!!.send(msg)
                    mSocket!!.close()
                } catch (e: SocketException) {
                    e.printStackTrace()
                }
            }
        }
    }

    fun closeClient() {
        mRun = false
        when (Role) {
            ERole.ROLE_Authority -> {
                sendMessage("Rejected")
                mService.onDataChanged()
                try {
                    if (mSocket != null) {
                        mSocket!!.close()
                    }
                } catch (e: SocketException) {
                    e.printStackTrace()
                }
            }
            ERole.ROLE_SimulatedProxy -> {

            }
            ERole.ROLE_AutonomousProxy -> {
                sendMessage("I went out")
                try {
                    mSocket!!.close()
                } catch (e: SocketException) {
                    e.printStackTrace()
                }
            }
        }
    }

    private fun sendAllPlayers() {
        if (Role == ERole.ROLE_Authority) {
            var str = "Players: "
            val service = mService as ServerService
            service.clientList
                    .filter { it.uuid != uuid && it.bIsAccepted }
                    .forEach { player -> str += (player.name + ", " + player.uuid + ", " + player.balance.toString() + "; ") }
            sendMessage(str)
        }
    }

    private fun sendMoney(fromPlayer: Player, count: Int) {
        if (count <= balance) {
            fromPlayer.balance += count
            this.balance -= count
            mService.operationList.add(Operation(this,fromPlayer,count))
            (mService as ServerService).multicastSend(this, "Operation: send money From: " +
                    this.uuid.toString() + " To: " + fromPlayer.uuid.toString() +
                    " Count: " + count.toString(), true)
        }
    }

    private fun sendToBankMoney(count: Int) {
        if (count <= balance) {
            this.balance -= count
            mService.operationList.add(Operation(this,mService.bank,count))
            (mService as ServerService).multicastSend(this, "Operation: send money From: " +
                    this.uuid.toString() + " To: " + "Bank" +
                    " Count: " + count.toString(), true)
        }
    }
}