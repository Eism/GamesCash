package ru.team.ae3.gamescash

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v7.app.AppCompatActivity


// многие классы наследуют этот класс
// он заранее создают фрагмент для активности, чтобы в классе самой аквтиности не писать

abstract class SingleFragmentActivity : AppCompatActivity() {

    companion object{
        val PREFS_NAME = "GameCashPrefsFile"
        val PERSON_NAME = "personName"
    }

    protected abstract fun createFragment(): Fragment


    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.content_fragment)
        val fm = supportFragmentManager
        var fragment: Fragment? = fm.findFragmentById(R.id.gridview)
        if (fragment == null) {
            fragment = createFragment()
            fm.beginTransaction().add(R.id.fragmentContainer1, fragment).commit()
        }


    }


}
