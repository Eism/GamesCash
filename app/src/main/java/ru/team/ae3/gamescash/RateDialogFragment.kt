package ru.team.ae3.gamescash


import android.app.Activity
import android.app.Dialog
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.TextInputLayout
import android.support.v4.app.DialogFragment
import android.support.v7.app.AlertDialog
import android.view.View
import android.widget.EditText

import android.view.WindowManager



class RateDialogFragment : DialogFragment(), DialogInterface.OnClickListener {

    private var form: View? = null
    private var form1: View? = null

    private var maxCash: Int? = null

    private var mListener: Listener? = null

    // ввод для диалога
    private var mTextInputLayout: TextInputLayout? = null
    private var mEditText: EditText? = null


    interface Listener {
        fun returnData(result: Int)
    }

    fun setListener(listener: Listener) {
        mListener = listener
    }


    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        maxCash = arguments.getInt(ARG_MAX_CASH)

        form = activity.layoutInflater.inflate(R.layout.dialog, null)
        val builder = AlertDialog.Builder(activity)
        return builder.setTitle("Ставка").setView(form)
                .setPositiveButton(android.R.string.ok, this)
                .setNegativeButton(android.R.string.cancel, null).create()
    }

    override fun onClick(dialog: DialogInterface, which: Int) {

        val rateBox = form!!.findViewById(R.id.rate) as EditText
        val rate = rateBox.text.toString()
        if (rate == "") return

        if (rate.toInt() > maxCash!!){

            requestFocus(rateBox)
            return
        }

        mListener!!.returnData(rate.toInt())
    }


    private fun showError() {
        //mTextInputLayout.error(getString(R.string.error))
    }






    private fun requestFocus(view: View) {
        if (view.requestFocus()) {
            (form!!.context as Activity).window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE)
        }
    }

    // метод, который отправляет данные целевому фрагменты, то есть PlayerContentFragment

    private fun sendResult(resultCode: Int, balance: Int) {
        if (targetFragment == null) {
            return
        }
        val intent = Intent()
        intent.putExtra(EXTRA_BALANCE, balance)
        intent.putExtra(ARG_MAX_CASH, maxCash)
        targetFragment.onActivityResult(targetRequestCode, resultCode, intent)
    }


    companion object {
        val EXTRA_BALANCE = "app.protector.com.mobilebank.balance"
        val ARG_MAX_CASH = "max cash"


        // инициализируем фрагмент (диалог) и передаём ему данные


        fun newInstance(maxCash: Int): RateDialogFragment {
            val args = Bundle()
            args.putSerializable(ARG_MAX_CASH, maxCash)
            val fragment = RateDialogFragment()
            fragment.arguments = args

            return fragment
        }
    }

}
