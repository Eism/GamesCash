package ru.team.ae3.gamescash

import android.os.*
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.FragmentManager

class BankContentFragment : Fragment() {

    var list : RecyclerView? = null
    var adapter = BankAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater!!.inflate(R.layout.fragment_bank_list, container, false)
        list = view.findViewById(R.id.game_recycler_view1) as RecyclerView

        list!!.layoutManager = LinearLayoutManager(context)
        list!!.adapter = adapter

        val bottomNavigationView = view.findViewById(R.id.navigation) as BottomNavigationView

        bottomNavigationView.setOnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.send_everyone -> {
                    var manager:FragmentManager? = activity.supportFragmentManager

                    val dialog = RateDialogFragment.newInstance(Int.MAX_VALUE)
                    dialog.setListener(object : RateDialogFragment.Listener{
                        override fun returnData(result: Int) {
                            ((activity as GameActivity).mService as ServerService).sendMoneyAllPlayers(result)
                        }

                    })
                    dialog.show(manager, "Give everyone")
                }
            }
            true
        }

        return view
    }

    override fun onResume() {
        super.onResume()
    }

}
