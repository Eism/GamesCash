/*
Copyright 2016 StepStone Services

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */

package ru.team.ae3.gamescash

import android.content.Context
import android.os.Bundle
import android.support.annotation.LayoutRes
import android.text.Html
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.Button

import com.stepstone.stepper.Step
import com.stepstone.stepper.VerificationError

class StepFragmentSample : ButterKnifeFragment(), Step {
    override val layoutResId: Int
        get() = arguments.getInt(LAYOUT_RESOURCE_ID_ARG_KEY)

    private var i = 0


    private var onNavigationBarListener: OnNavigationBarListener? = null

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is OnNavigationBarListener) {
            onNavigationBarListener = context as OnNavigationBarListener?
        }
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (savedInstanceState != null) {
            i = savedInstanceState.getInt(CLICKS_KEY)
        }

        updateNavigationBar()

    }
    override fun verifyStep(): VerificationError? {
        return if (isAboveThreshold) null else VerificationError("Click " + (TAP_THRESHOLD - i) + " more times!")
    }

    private val isAboveThreshold: Boolean
        get() = i >= TAP_THRESHOLD

    override fun onSelected() {
        updateNavigationBar()
    }

    override fun onError(error: VerificationError) {

    }

    private fun updateNavigationBar() {
        if (onNavigationBarListener != null) {
            onNavigationBarListener!!.onChangeEndButtonsEnabled(isAboveThreshold)
        }
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        outState!!.putInt(CLICKS_KEY, i)
        super.onSaveInstanceState(outState)
    }

    companion object {

        private val CLICKS_KEY = "clicks"

        private val TAP_THRESHOLD = 2

        private val LAYOUT_RESOURCE_ID_ARG_KEY = "messageResourceId"

        fun newInstance(@LayoutRes layoutResId: Int): StepFragmentSample {
            val args = Bundle()
            args.putInt(LAYOUT_RESOURCE_ID_ARG_KEY, layoutResId)
            val fragment = StepFragmentSample()
            fragment.arguments = args
            return fragment
        }
    }

}
