package ru.team.ae3.gamescash

import android.content.Intent
import android.os.*
import java.net.*
import android.os.Messenger

/**
 * Created by ELNUR on 08.03.2017.
 */

class ClientService: AbstractService()  {
    companion object{
        val TCP_ADDRESS = "tcp_address"
        var MESSAGER = "init messager"
        val PLAYER_NAME = "player name"
    }

    override var mActivity: Messenger? = null


    override fun onBind(p0: Intent?): IBinder {
        return mMessenger.binder
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {

        if (intent != null) {
            val tcpAddress = intent!!.getSerializableExtra(TCP_ADDRESS) as InetAddress
            mActivity = intent.getParcelableExtra(MESSAGER)
            val name = intent.getStringExtra(PLAYER_NAME)
            Thread(Runnable {
                clientList.add(Player(Player.ERole.ROLE_AutonomousProxy, this@ClientService, Socket(tcpAddress, 30015), name))
            }).start()
        }else{
            stopAService()
        }
        return super.onStartCommand(intent, flags, startId)
    }
}


