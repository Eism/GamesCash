package ru.team.ae3.gamescash

import android.app.Service
import android.content.Context
import android.content.Intent
import android.net.wifi.WifiManager
import android.os.*
import android.util.Log
import java.net.*
import java.util.*
import kotlin.concurrent.timerTask

/**
 * Created by Ericos on 06.03.2017.
 */

class SearchServerService : Service() {

    companion object{
        val MESSENGER = "messenger"
        val MSG_REGISTER_CLIENT = 1
        val START_SEARCHING = 2
    }
    private val mUdpSocket = DatagramSocket(null)
    var udpPort = 25565
    var ServerList = ArrayList<Server>()

    var mActivity: Messenger? = null
    val mMessenger = Messenger(IncomingHandler())

    override fun onCreate() {
        super.onCreate()
        mUdpSocket.bind(InetSocketAddress(udpPort))
        mUdpSocket.soTimeout = 5000
    }

    override fun onBind(p0: Intent?): IBinder {
        return mMessenger.binder
    }

    fun onDataChanged(){
        val message = Message()
        message.what = SearchServersActivity.MSG_DATA_CHANGED
        message.obj = this@SearchServerService
        mActivity!!.send(message)
    }

    fun startSearchingService(){
        ServerList.clear()
        Thread(Runnable {
            sendBroadcastPacket("Search server")
            Thread(Runnable {
                var timeIsUp = false
                var timer = Timer()
                timer.schedule(timerTask {
                    timeIsUp = true
                },5000)
                while (!timeIsUp){
                    var buf = ByteArray(255)
                    var packet = DatagramPacket(buf,buf.size)
                    try {
                        mUdpSocket.receive(packet)
                        var str = String(packet.data)
                        if(str.contains("I server")){
                            str = str.replace("I server ", "")
                            var server = Server(str, packet.address)
                            ServerList.add(server)
                            onDataChanged()
                        }
                    }catch (e : SocketException){
                        e.printStackTrace()
                    }catch (e : SocketTimeoutException){

                    }

                }
            }).start()
        }).start()

    }

    private fun sendBroadcastPacket(arg1 : String){
        var packet = DatagramPacket(arg1.toByteArray(),arg1.length,getBroadcastAddress(),25566)
        mUdpSocket.send(packet)
    }

    private fun getBroadcastAddress() : InetAddress{
        val wifi = getSystemService(Context.WIFI_SERVICE) as WifiManager
        val DHCP = wifi.dhcpInfo
        val broadcast = (DHCP.ipAddress and DHCP.netmask) or DHCP.netmask.inv()
        val quads = ByteArray(4)
        for (k in 0..3)
            quads[k] = ((broadcast shr k * 8) and 0xFF).toByte()
        return InetAddress.getByAddress(quads)
    }

    internal inner class IncomingHandler : Handler(){
        override fun handleMessage(msg: Message?) {
            Log.i("Client Msg",msg!!.what.toString())
            when (msg.what) {
                MSG_REGISTER_CLIENT ->{
                    mActivity = msg.replyTo
                }
                START_SEARCHING -> startSearchingService()
                else -> super.handleMessage(msg)
            }
        }
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {

        //mActivity = intent!!.getParcelableExtra(MESSENGER)
        return super.onStartCommand(intent, flags, startId)
    }

    override fun onDestroy() {
        super.onDestroy()
        mUdpSocket.close()
    }
}

