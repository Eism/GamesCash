package ru.team.ae3.gamescash;

import android.content.Intent;
import android.support.v4.app.Fragment;


public class GameExistActivity extends SingleFragmentActivity {


     // это когда мы нажимаем load в главном меню, и показываются сущ. игры , обратите внимание, от какого она класса наследуется
    // сотрите этот класс


    @Override
    protected Fragment createFragment() {
        return new GameListFragment();
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        Intent intent = new Intent(GameExistActivity.this, MainActivity.class);
//        startActivity(intent);
    }

}
