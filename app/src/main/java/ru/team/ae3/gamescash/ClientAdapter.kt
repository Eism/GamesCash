package ru.team.ae3.gamescash

import android.os.Handler
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import at.markushi.ui.CircleButton
import java.util.HashMap


class ClientAdapter : RecyclerView.Adapter<ClientAdapter.ClientAdapterViewHolder>() {

    var bIsServer = true

    var data = ArrayList<Player>()

    override fun onBindViewHolder(holder: ClientAdapterViewHolder?, position: Int) {
        holder!!.clientName.text = (data[position].name + "#" + data[position].uuid.toString().subSequence(9, 13))

        if (bIsServer) {
            //holder.rejectButton!!.setOnClickListener {
            //    if (!data[position].bIsAccepted) {
             //       data[position].acceptClient(false)
            //    } else {
            //        data[position].kickClient("Not reason")
            //    }
           // }
            holder.acceptButton!!.setOnClickListener {
                if (!data[position].bIsAccepted) {
                    holder.acceptButton!!.setBackgroundResource(R.drawable.ic_action_tick_black)
                    data[position].acceptClient(true)
                }
            }
        }


    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ClientAdapterViewHolder {

        val v :View
        if (bIsServer) v = LayoutInflater.from(parent!!.context).inflate(R.layout.client_item_for_server, parent, false)
        else v = LayoutInflater.from(parent!!.context).inflate(R.layout.client_item_for_client, parent, false)
        return ClientAdapterViewHolder(v, bIsServer)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    inner class ClientAdapterViewHolder(v: View, bIsServer: Boolean) : RecyclerView.ViewHolder(v) {
        var view = v
        var clientName = view.findViewById(R.id.client_name) as TextView
        var acceptButton: Button? = null
        var rejectButton: Button? = null

        private val handler = Handler() // hanlder for running delayed runnables
        var pendingRunnables = HashMap<String, Runnable>() // map of items to pending runnables, so we can cancel a removal if need be


        init {
            if (bIsServer) {
                acceptButton = view.findViewById(R.id.accept_button) as Button
            }
        }
    }


    fun remove(position: Int) {
        val item = data[position]

        if (data.contains(item)) {
            //data.removeAt(position)
            //notifyItemRemoved(position)
            if (!item.bIsAccepted) {
                item.acceptClient(false)
            } else {
                item.kickClient("Not reason")
            }
        }

    }
}