package ru.team.ae3.gamescash

import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.os.Message
import android.support.v4.app.Fragment
import android.view.View
import android.widget.Button
import at.markushi.ui.CircleButton


class LobbyActivity : SingleFragmentActivity() {

    companion object {
        val MODE = "server or client mode"
        val PERSON_NAME = "persone name"
    }
    var bIsServer = true
    var personName = ""

    var lobbyRecyclerFragment : LobbyRecyclerFragment? = null


    var btnStartGame: CircleButton? = null

    override fun createFragment(): Fragment {
        lobbyRecyclerFragment = LobbyRecyclerFragment()
        return lobbyRecyclerFragment as LobbyRecyclerFragment
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bIsServer = intent.getBooleanExtra(LobbyActivity.MODE, true)
        personName = intent.getStringExtra(LobbyActivity.PERSON_NAME)


    }

    override fun onBackPressed() {
        val builder = AlertDialog.Builder(this)

        builder.setMessage("Are you sure you want to exit?")
                .setCancelable(false)
                .setPositiveButton("Yes") { dialog, which ->
                    run {
                        // убить всех

                        val msg = Message()
                        msg.what = AbstractService.EActivityMessages.MSG_STOP_SERVICE.ordinal
                        lobbyRecyclerFragment?.mMesseger!!.send(msg)

                        val intent = Intent(applicationContext,MainActivity::class.java)
                        startActivity(intent)

                    }
                }
                .setNegativeButton("No") { dialog, which -> dialog!!.cancel() }
        val alert = builder.create()
        alert.show()
    }
}
