package ru.team.ae3.gamescash

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView

/**
 * Created by ELNUR on 19.03.2017.
 */
class OperationAdapter : RecyclerView.Adapter<OperationAdapter.OperationAdapterViewHolder>(){


    var data = ArrayList<Operation>()

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): OperationAdapter.OperationAdapterViewHolder {
        val v: View = LayoutInflater.from(parent!!.context).inflate(R.layout.item_operation, parent, false)
        return OperationAdapterViewHolder(v)
    }

    override fun onBindViewHolder(holder: OperationAdapter.OperationAdapterViewHolder?, position: Int) {
        holder!!.operationText.text = position.toString() + ". "+
                data[position].to.name + " перевел " +
                data[position].from.name + " " +
                data[position].count
    }

    override fun getItemCount(): Int {
        return data.size
    }


    inner class OperationAdapterViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        var view = v

        var operationText = view.findViewById(R.id.operation) as TextView
    }
}