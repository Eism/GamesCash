package ru.team.ae3.gamescash


import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

class OperationFragment : DialogFragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    var adapter = OperationAdapter()

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        var view = inflater!!.inflate(R.layout.fragment_operation, container, false)
        dialog.setTitle("Операции")

        var list = view.findViewById(R.id.list_view) as RecyclerView
        val btnBack = view.findViewById(R.id.button_back)

        list.layoutManager = LinearLayoutManager(context)
        list.adapter = adapter

        btnBack.setOnClickListener {
            dismiss()
        }

        return view
    }

}// Required empty public constructor
