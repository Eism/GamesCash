package ru.team.ae3.gamescash

import android.os.Bundle
import android.support.design.widget.BottomSheetDialogFragment
import android.support.design.widget.FloatingActionButton
import android.support.v7.app.AppCompatActivity

class MainActivity : AppCompatActivity() {
    var bottomSheetDialogFragment : BottomSheetDialogFragment? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        bottomSheetDialogFragment = NewGameBottomSheetFragment()
        (findViewById(R.id.floating_action_new_game) as FloatingActionButton).setOnClickListener {
            bottomSheetDialogFragment?.show(supportFragmentManager,bottomSheetDialogFragment?.tag)
        }
    }


    companion object {
        private val SPLASH_TIME_OUT = 1000
    }
}
