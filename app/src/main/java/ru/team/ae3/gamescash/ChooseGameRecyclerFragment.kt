package ru.team.ae3.gamescash

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

import com.stepstone.stepper.Step
import com.stepstone.stepper.VerificationError

interface ChooseGameSelect {
    fun Done()
}

class ChooseGameRecyclerFragment : Fragment(), Step {
    override fun onSelected() {

    }

    override fun verifyStep(): VerificationError? {
        return VerificationError("")
    }

    override fun onError(error: VerificationError) {

    }



    private var recyclerView: RecyclerView? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)

        retainInstance = true


    }


    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view = inflater!!.inflate(R.layout.activity_choose_game2, container, false)

        recyclerView = view.findViewById(R.id.choose_game_recycler) as RecyclerView
        val adapter = ContentAdapter()
        recyclerView!!.adapter = adapter
        recyclerView!!.setHasFixedSize(true)
        recyclerView!!.layoutManager = LinearLayoutManager(activity)


        return view
    }


    // число элементов в RecyclerView.


    internal inner class ContentAdapter : RecyclerView.Adapter<ContentAdapter.ChooseGameViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChooseGameViewHolder {
            return ChooseGameViewHolder(LayoutInflater.from(parent.context), parent)
        }

        override fun onBindViewHolder(holder: ChooseGameViewHolder, position: Int) {
            holder.title_name.text = "Monopoly"

            holder.itemView.setOnClickListener {
                (activity as ChooseGameSelect).Done()
            }

        }

        override fun getItemCount(): Int {
            return 1
        }

        inner class ChooseGameViewHolder(inflater: LayoutInflater, parent: ViewGroup) : RecyclerView.ViewHolder(inflater.inflate(R.layout.list_item_bank, parent, false)){
            var title_name = itemView.findViewById(R.id.title_text) as TextView
        }

    }
}





